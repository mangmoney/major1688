export const state = () => ({
  displayLoginPopup: false,
})

export const mutations = {
  toggleLogin(state) {
    state.displayLoginPopup = !state.displayLoginPopup
  },
}
