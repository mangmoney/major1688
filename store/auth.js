export const state = () => ({
  isLoggedIn: true,
})

export const mutations = {
  login(state) {
    state.isLoggedIn = true
  },
  logout(state) {
    state.isLoggedIn = false
  },
}
